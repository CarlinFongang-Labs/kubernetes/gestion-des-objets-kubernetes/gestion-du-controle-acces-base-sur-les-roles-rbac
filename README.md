# Gestion du contrôle d'accès basé sur les rôles dans Kubernetes


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

1. **Comprendre le Contrôle d'Accès Basé sur les Rôles (RBAC) dans Kubernetes**  
   Expliquer le rôle et l'importance du **RBAC** dans la gestion des permissions au sein d'un cluster Kubernetes.

2. **Identifier les Objets de l'API Kubernetes pour le RBAC**  
   Apprendre à connaître les objets principaux du RBAC, à savoir les rôles (Roles) et les rôles de cluster (ClusterRoles), ainsi que les liens de rôles (RoleBindings) et les liens de rôles de cluster (ClusterRoleBindings).

3. **Créer et Manipuler des Objets RBAC**  
   Savoir comment créer des rôles et des RoleBindings pour définir et appliquer des permissions dans Kubernetes.

4. **Appliquer et Vérifier les Permissions RBAC**  
   Utiliser `kubectl apply` pour créer et appliquer des fichiers de configuration YAML pour les rôles et les RoleBindings, et vérifier leur impact sur les permissions des utilisateurs.

5. **Tester les Modifications de RBAC dans un Environnement Pratique**  
   Interagir avec un cluster Kubernetes pour voir comment les modifications des rôles et des RoleBindings affectent les capacités des utilisateurs, en utilisant un laboratoire pratique fourni.

Ces objectifs permettent de maîtriser la gestion des permissions dans Kubernetes en utilisant le contrôle d'accès basé sur les rôles.

# 1. Qu'est-ce que le Contrôle d'Accès Basé sur les Rôles (RBAC) dans Kubernetes ?
Le contrôle d'accès basé sur les rôles permet de contrôler ce que les utilisateurs sont autorisés à faire et à accéder au sein d'un cluster Kubernetes. Par exemple, **RBAC** peut être utilisé pour permettre aux développeurs de lire les métadonnées et les journaux des pods sans leur permettre de modifier les pods.

# 2. Objets API de Kubernetes pour le RBAC
Les objets de l'API Kubernetes qui composent le système RBAC sont essentiels. 

Voici les objets principaux :

1. **Rôles et Rôles de Cluster (Roles et ClusterRoles)** : Ces objets définissent un ensemble de permissions. 

    - Un **rôle (Role)** définit des permissions spécifiquement au sein d'un namespace particulier, 
    - tandis qu'un **rôle de cluster (ClusterRole)** définit des permissions à l'échelle du cluster, sans se limiter à un namespace.

2. **Liens de Rôles et Liens de Rôles de Cluster (RoleBindings et ClusterRoleBindings)** : Ces objets lient les utilisateurs aux rôles. 

    - Les **RoleBindings** lient les utilisateurs aux rôles définis dans un namespace, 
    - tandis que les **ClusterRoleBindings** lient les utilisateurs aux rôles définis à l'échelle du cluster.

# 3. Démo

[Consultez le lab relatif à cette présentation en suivant ce lien](https://gitlab.com/CarlinFongang-Labs/kubernetes/gestion-des-objets-kubernetes/lab-gestion-du-controle-d-acces-base-sur-les-roles-rbac.git)

Une démonstration pratique de la gestion des objets RBAC est fournie. 

# 4. Recommandation de Lab Pratique
Il est recommandé de consulter le laboratoire pratique de cette section qui traite du contrôle d'accès basé sur les rôles. Ce laboratoire fournit un cluster de base avec lequel interagir et permet de tester les changements RBAC en tant qu'utilisateur supplémentaire. Ce laboratoire pratique aide à comprendre comment les modifications apportées aux rôles et aux RoleBindings affectent ce que l'utilisateur peut ou ne peut pas faire.

# Résumé
Ce laboratoire couvre le contrôle d'accès basé sur les rôles dans Kubernetes, les objets RBAC, et une démonstration pratique de la création et de la manipulation de ces objets.

C'est tout pour ce laboratoire. N'hésitez pas à consulter le laboratoire pratique pour approfondir la gestion du contrôle d'accès basé sur les rôles dans Kubernetes.



# Reference 

https://kubernetes.io/docs/reference/access-authn-authz/rbac/